import RPi.GPIO as GPIO
from flask import Flask, render_template
import datetime
app = Flask(__name__)
GPIO.setmode(GPIO.BCM)

pins = {
   17 : {'name' : 'salon', 'state' : GPIO.LOW},
   18 : {'name' : 'oturma', 'state' : GPIO.LOW},
   27 : {'name' : 'mutfak', 'state' : GPIO.LOW}
   }

for pin in pins:
   GPIO.setup(pin, GPIO.OUT)
   GPIO.output(pin, GPIO.LOW)

@app.route("/")
def main():
   # For each pin, read the pin state and store it in the pins dictionary:
   for pin in pins:
      pins[pin]['state'] = GPIO.input(pin)
   # Put the pin dictionary into the template data dictionary:
   templateData = {
      'pins' : pins
      }
   # Pass the template data into the template main.html and return it to the user
   return render_template('index.html', **templateData)

@app.route("/<changePin>/<action>")
def action(changePin, action):
	changePin = int(changePin)
	if action == "on":
		GPIO.output(changePin, GPIO.HIGH)
	if action == "off":
		GPIO.output(changePin, GPIO.LOW)

	templateData = {
      'pins' : pins
   }
	return render_template('index.html', **templateData)

if __name__ == "__main__":
   app.run(host='0.0.0.0', port=80, debug=True)